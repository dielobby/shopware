default['shopware']['ip_address'] = false
default['shopware']['hostname'] = false
default['shopware']['webroot'] = '/var/www'
default['shopware']['site_configuration_name'] = 'vagrant'
default['shopware']['server_aliases'] = 'www.site.dev'
default['shopware']['web_directory'] = ''
default['shopware']['allow_http2'] = false
default['shopware']['repository'] = ''
default['shopware']['repository_hostname'] = ''
default['shopware']['repository_port'] = 22
default['shopware']['download_shopware'] = false
default['shopware']['initialize_browser_testing'] = true
default['shopware']['browser_testing_directory'] = 'browser_testing'
default['shopware']['initialize_commit_validation'] = true
default['shopware']['commit_validation_packages'] = 'phpro/grumphp-shim "squizlabs/php_codesniffer=*" friendsofphp/php-cs-fixer phpspec/phpspec overtrue/phplint jakub-onderka/php-parallel-lint sensiolabs/security-checker'

if node['platform_version'] == '16.04'
	default['shopware']['mysql_version'] = '5.7'
else
	default['shopware']['mysql_version'] = '5.6'
end

default['shopware']['shopware_version'] = '^5.6'

# Example:
#[
#	{
#		:user => 'vagrant',
#		:hostname => 'deploy.site.com',
#		:port => 22,
#		:remote => '~/site/fileadmin/',
#		:local => 'fileadmin',
#		:sync => true
#	},
#	{
#		:user => 'vagrant',
#		:hostname => 'deploy.site.com',
#		:port => 22,
#		:remote => '~/site/uploads/',
#		:local => 'uploads',
#		:sync => true
#	}
#]
default['shopware']['sync_directories'] = []

# Example:
#[
#	{
# :database_name => 'shopware',
# :database_user => 'shopware',
#	:database_password => 'shopware',
# :dump_user => 'deploy',
# :dump_hostname => 'deploy.site.com',
# :dump_port => 22,
# :dump_remote => '~/shopware/dump.sql',
# :dump_local => '/home/vagrant/dump.sql',
# :post_install_queries => [
# 	"INSERT INTO shopware.shopware (id, roleID, username, password, encoder, localeID, name, email, active) VALUES (0, 1, 'shopware', '$2y$10$Uvf3Yfmmu40sM5iooRbMAOGw.UjfMd.KOeQ.AR2e4wOY6KLkDfV2O', 'bcrypt', 1, 'Admin', 'admin@shopware.de', 1)"
# ]
#	}
#]
default['shopware']['sync_databases'] = []

# Example:
# [
#   {
#       :source => '/var/www/mySite/fileadmin',
#       :target => '/var/www/fileadmin',
#   }
# ]
default['shopware']['create_links'] = []
